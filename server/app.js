var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

var oauthController = require('./routes/oauth2');
app.use('/oauth2', oauthController);

module.exports = app;

console.log("------------------");
