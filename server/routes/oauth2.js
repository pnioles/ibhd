var express = require('express');
var https = require('https');
var router = express.Router();

var remote_host = "https://developers.bancsabadell.com";
var first_path = "AuthServerBS/oauth/authorize";
var second_path = "AuthServerBS/oauth/token";
var client_id = "CLI9311358384843zesxvaqmubywificxzsfbcnkrktdzfdrckerwdki";
var client_secret = "C4ligul4s";
var redirect_uri = "http://localhost:3000/oauth2";
var state = "8ffdeaf9-bb78-421c-a639-491095352e77";
var auth = new Buffer(client_id+":"+client_secret).toString('base64');

var request = require('request');
var querystring = require('querystring');
var promise = require("promise");
var cheerio = require('cheerio');
var sslRootCAs = require('ssl-root-cas/latest')
sslRootCAs.inject()

var step2 = function(code, state_received){
	return new promise(function(resolve, reject){ 
		if(state_received != state) return false;
		var self = this;

		var options = {
		  host: 'developers.bancsabadell.com',
		  method: 'POST',
		  path: '/AuthServerBS/oauth/token?'+
		  	'grant_type=authorization_code&' +
		  	'code='+code+'&'+
		  	'redirect_uri='+encodeURI(redirect_uri),

		  headers: {'Authorization': 'Basic ' + auth, 'Accept':'application/json'},
		  rejectUnauthorized: false
		};

		var req = https.get(options, function(res) {
		  console.log('STATUS: ' + res.statusCode);
	  	console.log('HEADERS: ' + JSON.stringify(res.headers));
	  	
	  	res.setEncoding('utf8');
	  	var whatToReturn = false;
	  	res.on('data', function (chunk) {
	  	  console.log('BODY: ' + chunk);
	  	  resolve(chunk);
	  	});
	  	   //console.log("~~~~~~~~~~~~~~~~~~~");
			//console.log(res);
			//console.log("~~~~~~~~~~~~~~~~~~~");
		});

	});
}


/* GET home page. */
router.get('/', function(req, res, next) {
	var params = req.query;

	if(!params.error){

		if(params.code){
			console.log("CODE: ", params.code);
			console.log("STATE: ", params.state);

			step2(params.code, params.state).then(function(result){
				if(result){
				 console.log(result);
				 res.status(200).send(result);
				}
				else res.status(401).send("UNAUTHORIZED");	
			});
			
		}
		else{
			res.status(200).send("OK");
			//implementar aquí el paso 1 que ahora lo hace la app de angular
		}
	}
	else{
		res.status(500).send("UNAUTHORIZED");
		console.log("ERROR: ", params.error_description);
	}
	console.log("-----------------------")
});

router.get('/access', function(req, res, next){
	process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
	user = req.query.usr;
	pass = req.query.pass;
	console.log("usr: "+user+" & pass: "+pass);
	url = "https://developers.bancsabadell.com/AuthServerBS/oauth/authorize/oauth/authorize?response_type=code&client_id=CLI9311358384843zesxvaqmubywificxzsfbcnkrktdzfdrckerwdki&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Foauth2&scope=read%20write&state=8ffdeaf9-bb78-421c-a639-491095352e77";
	new promise(function (resolve, reject) {
		request({
			"url":url,
			"rejectUnauthorized": false
		}, function (error, response, html){
			if(error){
				console.log(error);
				reject(error);
			}
			//console.log(html);
			resolve(true);
		});
	}).then(function(result){
		return new promise(function(resolve, reject){
			url = "https://developers.bancsabadell.com/AuthServerBS/j_spring_security_check";
			form = {
				j_product:"apibs.authserver.login.producto.particular.procedencia",
				j_acceso:"ID_NIF",
				j_client_id:"CLI9311358384843zesxvaqmubywificxzsfbcnkrktdzfdrckerwdki",
				j_username:user,
				j_password:pass
			};
			formData = querystring.stringify(form);
			var contentLength = formData.length;
			request.post({
				url:url, 
				"rejectUnauthorized": false, 
				"followAllRedirects": false,
				headers: {
					'Accept':"*/*",
					'Content-Length': contentLength,
					'Content-Type': 'application/x-www-form-urlencoded',
					'Connection': 'keep-alive',
					'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36',
				},
				body: formData,
				method: "POST"
			}
			, function(error, response, html){
				if(error){
					console.log(error);
				}
				console.log("casi...");
				console.log(response.headers['set-cookie'][0].split(";")[0]);
				//console.log(html);
				resolve(response.headers['set-cookie'][0].split(";")[0]);
			});
			
		})	
	},
	function(error){
		res.status(400).send("ERROR");	
	})
	.then(function(result){
		//res.status(200).send("OMG");		
		
		return new promise(function(resolve, reject){
			url = "https://developers.bancsabadell.com/AuthServerBS/oauth/authorize/oauth/authorize?response_type=code&client_id=CLI9311358384843zesxvaqmubywificxzsfbcnkrktdzfdrckerwdki&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Foauth2&scope=read%20write&state=8ffdeaf9-bb78-421c-a639-491095352e77";
			request({
				"url":url,
				"rejectUnauthorized": false,
				"followAllRedirects": true,
				"method":"GET",
				headers: {
					"Cookie":result,
					'Connection': 'keep-alive',
					'Referer':'https://developers.bancsabadell.com/AuthServerBS/login'
				},
				
			},function(error, response, html){
				if(error){
					console.log(error);
				}
				console.log("casi casi..."+result);
				//console.log(html);
				resolve(result);	
			});
		});
		
		
	}).then(function(result){
		console.log("redo");
		//res.status(200).send(response);		
		return new promise(function(resolve, reject){
			form = {
				'user_oauth_approval':"true"
			}
			formData = querystring.stringify(form);
			var contentLength = formData.length;
			url = "https://developers.bancsabadell.com/AuthServerBS/oauth/authorize/oauth/authorize?response_type=code&client_id=CLI9311358384843zesxvaqmubywificxzsfbcnkrktdzfdrckerwdki&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Foauth2&scope=read%20write&state=8ffdeaf9-bb78-421c-a639-491095352e77";
			request({
				"url":url,
				"rejectUnauthorized": false,
				"followAllRedirects": false,
				"method":"POST",
				headers: {
					"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
					"Cookie":result,
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': contentLength,
					'Connection': 'keep-alive',
					'Referer':'https://developers.bancsabadell.com/AuthServerBS/oauth/authorize/oauth/authorize?response_type=code&client_id=CLI9311358384843zesxvaqmubywificxzsfbcnkrktdzfdrckerwdki&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Foauth2&scope=read%20write&state=8ffdeaf9-bb78-421c-a639-491095352e77'
				},
				body: formData
				
			},function(error, response, html){
				if(error){
					console.log(error);
				}
				console.log("casi casi...");
				console.log(response.headers.location);
				//console.log(html);
				html = html.replace(/\/StaticOauth/g, 'https://developers.bancsabadell.com/StaticOauth');
				html = html.replace(/authorize/g, "https://developers.bancsabadell.com/AuthServerBS/oauth/authorize");
				resolve(response.headers.location);	
			});
		});
	}).then(function(result){
		res.redirect(result);
		//res.status(200).send(result);		
	});
	
})

module.exports = router;
