window.onload = function () {

    // add eventListener for tizenhwkey
    document.addEventListener('tizenhwkey', function(e) {
        if(e.keyName == "back")
		try {
		    tizen.application.getCurrentApplication().exit();
		} catch (ignore) {
	}
    });   
};


var ibhd = angular.module('ibhd',['ui.router']);

var ibhdCtrl = {}; ibhd.controller(ibhdCtrl);

ibhd.config(function ($stateProvider,$urlRouterProvider) {
	  
	  $stateProvider
	    .state("contacts", {
	      url: '/contacts',
	      templateUrl: 'views/contacts/main.html',
	      controller: 'ContactsController'
	    })
	    .state("mainMenu", {
	      url: '/mainmenu',
	      templateUrl: 'views/mainMenu/main.html',
	      controller: 'MainMenuController'
	    })
	    .state("house", {
	      url: '/house',
	      templateUrl: 'views/house/main.html',
	      controller: 'HouseController'
	    });	
	  
	  
	  $urlRouterProvider.otherwise("/house");
});
