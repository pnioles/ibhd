window.onload = function () {

    // add eventListener for tizenhwkey
    document.addEventListener('tizenhwkey', function(e) {
        if(e.keyName == "back")
		try {
		    tizen.application.getCurrentApplication().exit();
		} catch (ignore) {
	}
    });   
};


var ibhd = angular.module('ibhd',['ui.router']);

var ibhdCtrl = {}; ibhd.controller(ibhdCtrl);
var ibhdFtry = {}; ibhd.factory(ibhdFtry);


ibhd.config(function ($stateProvider,$urlRouterProvider) {
	  
	  $stateProvider
	    .state("contacts", {
	      url: '/contacts',
	      templateUrl: 'views/contacts/main.html',
	      controller: 'ContactsController'
	    })
	    .state("mainMenu", {
	      url: '/mainmenu',
	      templateUrl: 'views/mainMenu/main.html',
	      controller: 'MainMenuController'
	    })
	    .state("slectionMenu", {
	      url: '/selectionmenu',
	      templateUrl: 'views/selectionMenu/main.html',
	      controller: 'SelectionMenuController'
	    })
	    .state("house", {
	      url: '/house',
	      templateUrl: 'views/house/main.html',
	      controller: 'HouseController'
	    });	
	  
	  
	  $urlRouterProvider.otherwise("/mainmenu");
});

ibhd.run(function(ParseService,$rootScope, $state){

	 ParseService.init();

	$rootScope.go = function (state,params){
		$state.go(state,params);
	}

	$rootScope.getImageRandom = function(){
		var num = Math.floor((Math.random() * 22) + 1);
		return 'images/'+num+'.jpg';
	}
});
