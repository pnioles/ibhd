ibhdCtrl.HouseController = function($rootScope, $scope){
	console.log("house controller");

	$scope.houses = [{"code": 987, "price": 34000, "published_at": "2008-11-27", "bedrooms": 4, "bathrooms": 1, "area": 69, "location": "Alicante/Alacant", "type": "Apartamento", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.375262, "longitude": -0.485611 }, "garage": false, "storage": false, "cp": "3014", "used": true, "year": 1970 }, {"code": 1032, "price": 36500, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 1, "area": 132, "location": "Mon„var/MonÖver", "type": "Casa Planta Baja", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.437815, "longitude": -0.834811 }, "garage": false, "storage": false, "cp": "3640", "used": true, "year": 1900 }, {"code": 1053, "price": 78400, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 1, "area": 95, "location": "Elche/Elx", "type": "Apartamento", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.263435, "longitude": -0.704107 }, "garage": false, "storage": false, "cp": "3201", "used": true, "year": 1973 }, {"code": 1070, "price": 30000, "published_at": "2008-11-27", "bedrooms": 4, "bathrooms": 1, "area": 90, "location": "Aspe", "type": "Piso", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.3462, "longitude": -0.767761 }, "garage": false, "storage": false, "cp": "3680", "used": true, "year": 1970 }, {"code": 1072, "price": 52400, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 1, "area": 101, "location": "Redován", "type": "Piso", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.137478, "longitude": -0.932374 }, "garage": false, "storage": false, "cp": "3370", "used": true, "year": 1989 }, {"code": 1123, "price": 25500, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 1, "area": 81, "location": "Alicante/Alacant", "type": "Piso", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.37551, "longitude": -0.49137 }, "garage": false, "storage": false, "cp": "3011", "used": true, "year": 1969 }, {"code": 1124, "price": 65000, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 1, "area": 119, "location": "Muro de Alcoy", "type": "Chalet adosado", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.784028, "longitude": -0.451823 }, "garage": false, "storage": false, "cp": "3830", "used": true, "year": 1975 }, {"code": 1145, "price": 19000, "published_at": "2008-11-27", "bedrooms": 5, "bathrooms": 2, "area": 114, "location": "Elda", "type": "Casa Planta Baja", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.482427, "longitude": -0.791987 }, "garage": false, "storage": false, "cp": "3600", "used": true, "year": 1933 }, {"code": 1156, "price": 31566.5, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 1, "area": 66, "location": "Elda", "type": "Piso", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.475504, "longitude": -0.785727 }, "garage": false, "storage": false, "cp": "3600", "used": true, "year": 1960 }, {"code": 1227, "price": 11000, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 1, "area": 63, "location": "Elda", "type": "Piso", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.480084, "longitude": -0.800202 }, "garage": false, "storage": false, "cp": "3600", "used": true, "year": 1960 }, {"code": 1319, "price": 37900, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 1, "area": 169, "location": "Crevillent", "type": "Casa", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.250326, "longitude": -0.809559 }, "garage": false, "storage": false, "cp": "3330", "used": true, "year": 1900 }, {"code": 1323, "price": 88000, "published_at": "2008-11-27", "bedrooms": 3, "bathrooms": 2, "area": 64, "location": "Torrevieja", "type": "Apartamento", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.006164, "longitude": -0.694523 }, "garage": false, "storage": false, "cp": "3184", "used": true, "year": 1998 }, {"code": 1456, "price": 55700, "published_at": "2008-12-10", "bedrooms": 3, "bathrooms": 1, "area": 75, "location": "Elche/Elx", "type": "Piso", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.276496, "longitude": -0.712202 }, "garage": false, "storage": false, "cp": "3206", "used": true, "year": 1973 }, {"code": 1472, "price": 36800, "published_at": "2008-12-10", "bedrooms": 2, "bathrooms": 1, "area": 53, "location": "Orihuela", "type": "Piso", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.082419, "longitude": -0.95521 }, "garage": false, "storage": false, "cp": "3300", "used": true, "year": 1962 }, {"code": 1494, "price": 46400, "published_at": "2008-12-11", "bedrooms": 1, "bathrooms": 1, "area": 50, "location": "Benidorm", "type": "Apartamento", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.544717, "longitude": -0.127813 }, "garage": false, "storage": false, "cp": "3502", "used": true, "year": 1976 }, {"code": 1538, "price": 94300, "published_at": "2008-12-15", "bedrooms": 3, "bathrooms": 2, "area": 82, "location": "Torrevieja", "type": "Apartamento", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 37.98227, "longitude": -0.667924 }, "garage": false, "storage": false, "cp": "3182", "used": true, "year": 2000 }, {"code": 1555, "price": 64800, "published_at": "2008-12-15", "bedrooms": 3, "bathrooms": 2, "area": 120, "location": "Orihuela", "type": "Piso", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 38.080586, "longitude": -0.942825 }, "garage": false, "storage": false, "cp": "3300", "used": true, "year": 1988 }, {"code": 1775, "price": 27500, "published_at": "2008-12-19", "bedrooms": 2, "bathrooms": 1, "area": 85, "location": "Alicante/Alacant", "type": "Piso", "province": "Alicante", "vpo": true, "geo":{"__type":"GeoPoint", "latitude": 38.374231, "longitude": -0.492539 }, "garage": false, "storage": false, "cp": "3011", "used": true, "year": 1968 }, {"code": 1787, "price": 73100, "published_at": "2008-12-19", "bedrooms": 2, "bathrooms": 1, "area": 59, "location": "Torrevieja", "type": "Apartamento", "province": "Alicante", "vpo": false, "geo":{"__type":"GeoPoint", "latitude": 37.980731, "longitude": -0.690631 }, "garage": false, "storage": false, "cp": "3182", "used": true, "year": 2001 } ]; 
	$scope.precioInput;


	this.getPropertys = function(){

		for(var i in $scope.houses)
		  		$scope.houses[i].img = $rootScope.getImageRandom();

		return true;

		var Property = Parse.Object.extend("property");
		var query = new Parse.Query(Property);
		query.limit(40);
		query.find({
		  success: function(results) {
		  

				$scope.houses = results;

				for(var i in $scope.houses )
		  		$scope.houses[i].attributes.img = $rootScope.getImageRandom();
		  	
				$scope.$apply();
		  },
		  error: function(error) {
		    console.log("Error: " + error.code + " " + error.message);
		  }
		});
	}
	
	this.getPropertys();

	/*
	var house = {
		 img:$rootScope.getImageRandom(),
		 bedrooms: 2,
		 bathrooms: 1,
		 area: 54.5,
		 location: "Torrevieja",
		 type: "Apartamento",
		 province: "Playa de san juan",
		 vpo: false,
		 geo:{
		 latitude: 37.978249,
		 longitude: -0.667572
		 },
		 garage: false,
		 storage: false,
		 cp: 3180,
		 used: true,
		 year: 1969
	}

	$scope.houses.push(house);
	$scope.houses.push(house);
	*/

	$scope.changePhoto = function(){
		$scope.houses[0].img = $rootScope.getImageRandom();
		console.log($scope.houses[0].img);
	}

	$scope.valorar = function(){
		if($scope.precioInput){
			$scope.houses.splice(0,1);
			$scope.precioInput = null;
		}
		else{
			$("#inputPrice").focus();
		}
	}
}