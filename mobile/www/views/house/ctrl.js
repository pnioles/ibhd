ibhdCtrl.HouseController = function($rootScope, $scope, $ionicSlideBoxDelegate,	$timeout){
	console.log("house controller");

	var self = this;
	$scope.houses = []
	$scope.priceValoration = null;

	var page = 0;
	var limit = 20;

	this.getPropertys = function(){

		var Property = Parse.Object.extend("property");
		var query = new Parse.Query(Property);
		query.limit(20);

		query.limit(limit);
		query.skip(page * limit);

		query.find({
		  success: function(results) {
				for(var i in results){
					$scope.houses.push(results[i]);
					$scope.houses[i].imgs = [$rootScope.getImageRandom(),$rootScope.getImageRandom(),$rootScope.getImageRandom()];					
		  		$scope.$apply();
		  	}
		  	
				$scope.$apply();
		  },
		  error: function(error) {
		    console.log("Error: " + error.code + " " + error.message);
		  }
		});
	}
	
	this.getPropertys();

	/*
	var house = {
		 img:$rootScope.getImageRandom(),
		 bedrooms: 2,
		 bathrooms: 1,
		 area: 54.5,
		 location: "Torrevieja",
		 type: "Apartamento",
		 province: "Playa de san juan",
		 vpo: false,
		 geo:{
		 latitude: 37.978249,
		 longitude: -0.667572
		 },
		 garage: false,
		 storage: false,
		 cp: 3180,
		 used: true,
		 year: 1969
	}

	$scope.houses.push(house);
	$scope.houses.push(house);
	*/

	$scope.getPriceValoration = function(){
		return priceValoration;
	}

	$scope.valorar = function(){

		$scope.valorado = true;
		$timeout(function(){
			$scope.valorado = false;
			$scope.houses.splice(0,1);
			$scope.priceValoration = null;
			if($scope.houses.length < 5)
				self.getPropertys();

		},1000);

		
	}
}