var ibhdCtrl = {};
var ibhdFtry = {};
var ibhdFiltr = {};
var ibhdDrctv = {};

angular
.module("ibhd", ['ui.router', 'ngMaterial'])
.controller(ibhdCtrl)
.factory(ibhdFtry)
.filter(ibhdFiltr)
.directive(ibhdDrctv)
.config( function($stateProvider, $urlRouterProvider){

  $stateProvider
	//----- Login -------- //
	.state("login", {
      onEnter: function(UserService, $state){
        if(UserService.getCurrent() != null)
          $state.go('app.dashboard');
      },
	    url: '/login',
	    views : {
		  	body:{
		  		templateUrl: 'src/app/auth/login.html',
		  		controller: 'LoginController'
		  	}
		  }
  	})
  .state("signup", {
      onEnter: function(UserService, $state){
        if(UserService.getCurrent() != null)
          $state.go('app.dashboard');
      },
      url: '/signup',
      views : {
        body:{
          templateUrl: 'src/app/auth/signup.html',
          controller: 'LoginController'
        }
      }
    })
  .state("resetpass", {
      onEnter: function(UserService, $state){
        if(UserService.getCurrent() != null)
          $state.go('app.dashboard');
      },
      url: '/resetpass',
      views : {
        body:{
          templateUrl: 'src/app/auth/resetpass.html',
          controller: 'LoginController'
        }
      }
    })
  	//----- App -------- //
	.state("app",{
    onEnter: function(UserService, $state, $rootScope){
      var user = UserService.getCurrent();
      if(user == null) $state.go('login');
      else $rootScope.mainUser = user;
      console.debug("Loged user: ", $rootScope.mainUser);
    },
		url : '',
    abstract:true,
		views : {
			body:{
				templateUrl: 'src/app/main.html',
				controller: 'AppController'
			}
		}
	})
	//----- main page app -------- //
    .state("app.dashboard", { //Pagina principal con los favoritos...
      url : '/dashboard',
      views : {
        appContent:{
          templateUrl: 'src/app/dashboard/main.html',
          controller: 'DashController'
        },
        appNavbar:{
          templateUrl: 'src/app/navbar.html',
          controller: ''
        }
      }
    })
	//----- User List app -------- //
    .state("app.users", {
      url : '/users',
      views : {
        appContent:{
          templateUrl: 'src/app/users/main.html',
          controller: 'UserController'
        },
        appNavbar:{
          templateUrl: 'src/app/navbar.html',
          controller: ''
        }
      }
    })
    //----- House List app -------- //
    .state("app.houses", {
      abstract: true,
      url : '/houses',
      views : {
        appContent:{
          templateUrl: 'src/app/houses/main.html',
          controller: 'HouseController'
        },
        appNavbar:{
          templateUrl: 'src/app/navbar.html',
          controller: ''
        }
      }
    })
    .state("app.houses.list", {
      url : '',
      views : {
        housesContent:{
          templateUrl: 'src/app/houses/list/main.html',
          controller: 'HouseListController'
        }
      }
    })
    .state("app.houses.detail", {
      url : '/detail?id',
      views : {
        housesContent:{
          templateUrl: 'src/app/houses/detail/main.html',
          controller: 'HouseDetailController'
        }
      }
    })

  $urlRouterProvider.otherwise("/login");
})
.run(function(ParseService, $rootScope){
  ParseService.init();

  $rootScope.getImageRandom = function(){
    var num = Math.floor((Math.random() * 22) + 1);
    return 'assets/img/'+num+'.jpg';
  }

  $rootScope.getRandom = function(max){
    var num = Math.floor((Math.random() * max) + 1);
    return num;
  }

  $rootScope.greaterThan = function(prop, val){
    return function(item){
      if (!val || item[prop] > val) return true;
    }
  }
})
