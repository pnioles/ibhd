var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
    '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

var isEmpty = function(value){
    return value == "" || value == null || value == undefined;
}
var isEmptyObject = function(obj){
  if (isEmpty(obj) || !(obj instanceof Object)){
    return true;
  }
  else{
    var keys = Object.keys(obj);
    if((keys.length==0) || (keys.length==1 && keys[0] == '$$hashKey'))
        return true;
    else{
        for (var i in obj) {
            if(i!='$$hashKey' && !isEmpty(obj[i])) return false;
        };
        return true;
    }
  }
}

var types = ['category','check', 'single', 'multiple', 'numeric', 'text', 'slider', 'draw', 'signature', 'category' ]
var getTypeById = function(id){
    if(types.length > id)
        return types[id];
    else return 'main';
}
var pad = function (n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
var getIdByType = function(type){
    switch(type){
      case 'category':  return 9; break;
      case 'check':     return 1; break;
      case 'single':    return 2; break;
      case 'multiple':  return 3; break;
      case 'numeric':   return 4; break;
      case 'text':      return 5; break;
      case 'slider':    return 6; break;
      case 'draw':      return 7; break;
      case 'signature': return 8; break;
      default:          return 0; break;
    }
}

var errorToast = function(msg){
  toastr.warning(msg);
}

var successToast = function(msg){
  toastr.success(msg);
}

var numberToBool = function(value){
    return value != 0;
}

var validEmail = function(email){
    var email_regex = new RegExp(REGEX_EMAIL);
    return email_regex.test(email);
}
var formatName = function(item) {
    return $.trim((item.first_name || '') + ' ' + (item.last_name || ''));
}
var toggle = function(array, value) {
    'use-strict';
    var index = array.indexOf(value);
    if (index === -1) {
        array.push(value);
    } else {
        array.splice(index, 1);
    }
}
var log = function(l){console.log(l);}
var debug = function(d){console.debug(d);}

function getBase64Image(imgId) {
  var canvas = document.createElement("canvas");
  var img = document.getElementById(imgId);
  //console.debug(img);
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  
  var dataURL = canvas.toDataURL('image/jpg');
  var base64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, ""); 
  return base64;
}

var mobileManuallySetted = {
    setted:false,
    value: null
}

var mobileSetManually = function(value){
    mobileManuallySetted.setted = true;
    mobileManuallySetted.value = value;
}

var isMobile = {
    Android:function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry:function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS:function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera:function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows:function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any:function () { //return true; //quitar esto
        if(mobileManuallySetted.setted)
            return mobileManuallySetted.value;
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()) ? true : false;
    }
};

var checkConnection = function() {

    var networkState = navigator.connection.type;
    var states = {};

    states[Connection.UNKNOWN]  = 'Unknown';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'None';

    return states[networkState];
}
function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

var hasConnection = function(){
    var connection = checkConnection();
    return connection != "None" && connection != "Unknown";
}

var getTemplateType = function () {
    //return isMobile.any() ? '.mobile' : '';
    return isMobile.any() ? '.mobile' : '';
}

var encodeURI = function(value){
    if(value === true) value = 1;
    else if(value === false) value = 0;
    else if(value === null) value = '';
    return encodeURIComponent(value);
}

var toFormData = function(o){
    var obj = clone(o);
    var data = [];
    for(var i in obj) {
        if(angular.isArray(obj[i])){
            for(var j in obj[i]) {
                data.push(i + '['+j+']=' + encodeURI(obj[i][j]));
            }
        } else {
            data.push(i + '=' + encodeURI(obj[i]));
        }
    }
    return data.join('&');
};

var visualSearch = function(container, data, searchMethod){
    return VS.init({
        container  : container,
        query      : '',
        minLength  : 0,
        showFacets : true,
        readOnly   : false,
        unquotable : Object.keys(data),
        placeholder : "Search by label, account or template",
        callbacks  : {
          valueMatches : function(category, searchTerm, callback) {if(data[category]) callback(data[category]); },
          facetMatches : function(callback) {callback(Object.keys(data));},
          search: searchMethod
        }
    });
}

var visualCKEditor = function(container, stringContent, callback){
    $("#"+container).append(stringContent);

    CKEDITOR.disableAutoInline = true;
    var editor = CKEDITOR.inline(container,{
      filebrowserBrowseUrl : 'src/vendor/ckeditor/plugins/ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl : 'src/vendor/ckeditor/plugins/ckfinder/ckfinder.html?type=Images',
      filebrowserFlashBrowseUrl : 'src/vendor/ckeditor/plugins/ckfinder/ckfinder.html?type=Flash',
      filebrowserUploadUrl : 'src/vendor/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl : 'src/vendor/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
      filebrowserFlashUploadUrl : 'src/vendor/ckeditor/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });


    $("#"+container).on("blur", function() {
        callback(editor.getData());
    });
}

var basicRating = function (container, enabled){
       container.jRating({
        length : 5, // nb of stars
        rateMax : 5,
        isDisabled: !enabled,
        onSuccess : function(){
           alert('Success : your rate has been saved :)');
        },
        onClick : function(element,rate) {
            alert(rate);
        }
       });    
}

var datepickerRange = function(container, options, result){

    var startDate = moment().format("DD/MM/YYYY");

    var defaultOptions = {
        singleDatePicker: false,
        format: 'DD/MM/YYYY',
        startDate: startDate,
        //showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
         'Last 7 Days': [moment().subtract('days', 6), moment()],
         'Last 30 Days': [moment().subtract('days', 29), moment()]
        },
        //opens: 'right',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD/MM/YYYY',
        separator: ' to ',
        locale: {
          applyLabel: 'Submit',
          cancelLabel: 'Clear',
          fromLabel: 'From',
          toLabel: 'To',
          //customRangeLabel: 'Single date',
          //daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
          //monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          //firstDay: 1
        }
    };

    //update options
    for (key in options) {
        defaultOptions[key] = options[key];
    }
    
    container.daterangepicker(defaultOptions,result);
}

var tagEditor = function(container, initialValues, onAdd, onDelete){
    container.masterblaster({//options
      animate: true,
      helpText: '',
      tagRules:{
          unique: true,
          minLength: 0
      },
      showAddButton: true
    });

    if(initialValues && initialValues.length>0){
        for(i in initialValues){
            container.masterblaster("push",initialValues[i].name);
        }
    }
    container.on( "mb:add", onAdd);  
    container.on( "mb:remove", onDelete); 
}

var imagePicker = function(containerId, buttonId, callback){
    var croppicHeaderOptions = {
      uploadUrl:'img_save_to_file.php',
      cropUrl:'img_crop_to_file.php',
      customUploadButtonId:buttonId,
      modal:false,
      zoomFactor:10,
      doubleZoomControls:false,
      onAfterImgCrop: callback,
      loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> '
    }
    var croppic = new Croppic(containerId, croppicHeaderOptions);
}

var categoriesSelector = function(container, data, onchange){
    var myOptions = [];
    var myCategories = [];
    for (var i = 0; i < data.length; i++) {
        var category = {};
        category.id = data[i].id;
        category.name = data[i].name;
        myCategories.push(category);

        for (var j = 0; j < data[i].children.length; j++) {
            var option = {};
            option.id = data[i].children[j].id;
            option.label = data[i].children[j].label;
            option.category = data[i].id;
            myOptions.push(option);
        };
    };
    container.selectize({
        maxItems: 10,
        options: myOptions,
        optgroups: myCategories,
        labelField: 'label',
        valueField: 'id',
        optgroupField: 'category',
        optgroupLabelField: 'name',
        optgroupValueField: 'id',
        searchField: ['label'],
        plugins: ['optgroup_columns', 'remove_button'],
        render: {
            item: function(item, escape) {
              switch(item.category){
                case 'acc': item.image = 'assets/images/icons/accounts_32x32.png'; break;
                case 'tmp': item.image = 'assets/images/icons/templates_32x32.png'; break;
                case 'tag': item.image = 'assets/images/icons/tag_32x32.png'; break;
              }
              return '<div>' +
                '<img height="20px" src="'+item.image+'">' +
                '<span>' + escape(item.label) + ' </span>' +
              '</div>';
            }
        },
        onChange: onchange
    });
}
var emailSelector = function(container,emails,onchange){
    container.selectize({
      persist: false,
      maxItems: null,
      valueField: 'email',
      labelField: 'name',
      searchField: ['name', 'email'],
      sortField: [
        {field: 'name', direction: 'asc'},
      ],
      options: emails,
      render: {
        item: function(item, escape) {
          var name = item.name;
          return '<div>' +
            (name ? '<span class="name">' + escape(name) + '</span>' : '') +
            (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
          '</div>';
        },
        option: function(item, escape) {
          var name = item.name;
          var label = name || item.email;
          var caption = name ? item.email : null;
          var img = item.img ? item.img : null;

          var item = '<div>' +
            (img ? '<img class="user-icon pull-left" width="40px" src="'+ escape(img) +'" alt="">' : '') +
            '<div class="pull-left" style="margin-left:15px"><span class="label">' + escape(label) + '</span>' +
            (caption ? '<span class="caption"> ' + escape(caption) + '</span>' : '') +
          '</div></div>';

          return item;
        }
      },
      create: function(input) {
        if ((new RegExp('^' + REGEX_EMAIL + '$', 'i')).test(input)) {
          return {email: input};
        }
        var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));
        if (match) {
          return {
            email: match[2],
            name: match[1],
          };
        }
        errorToast('Invalid email address.');
        return false;
      },
      onChange: onchange
    });
}
var calendar = function(id, eventsURL){

    $('#'+id).fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: '2014-01-12',
        firstDay: 1,
        aspectRatio: Math.max(1.5,Math.round(screen.width/screen.height, 2)),
        weekNumbers: true,
        selectable: true,
        selectHelper: true,
        editable: true,
        events: {
            url: eventsURL,
            error: function(e) {
                console.log(e);
                $('#script-warning').show();
            }
        },
        loading: function(bool) {
            $('#loading').toggle(bool);
        }
    });
}
var imgToDrawable = function (idCont, urlImg, save) {
    this.saveFunc = save;
    this.init(idCont, urlImg);
}
imgToDrawable.prototype = {
    constructor: imgToDrawable, 
    
    saveFunc: null,
    idCont: null,
    cont: null, 
    
    urlImg: null,
    widthImg: null,
    heightImg: null,

    img: null,
    canvas: null,
    signaturePad: null,

    btn1: null,
    btn2: null,
    btn3: null,
    btn4: null,

    init: function(idCont, urlImg){
        this.idCont = this.idCont || idCont;
        this.urlImg = this.urlImg || urlImg;
        this.cont = this.cont || $("#"+idCont);
        this.cont.empty();

        this.getImgSize();
    },
    initContinue: function(){
        //when finish load size img
        this.img = $("<img />");
        this.img.attr('src', this.urlImg);
        this.img.addClass('signature');

        this.img.attr('width', this.widthImg+'px');
        this.img.attr('height', this.heightImg+'px');

        this.cont.append(this.img);
        this.btn1 = $('<button>Edit</button>');
        this.btn1.addClass('btn btn-info');
        this.btn1.css({
            'position': 'absolute',
            'left': '0',
            'margin-left': '60px',
            'margin-top':'-34px'
        });
        this.cont.append(this.btn1);
        var self = this;
      
        this.btn1.click(function(event) {
          self.drawable();}
        );
        self.drawable();
    },
    drawable: function() {
        var self = this;
        var btnGroup = $('<div class="btn-group draw-controls fr-mobile mt10-mobile relative"> </div>');
        this.btn2 = $('<button>&nbsp;Clear&nbsp;</button>');
        this.btn2.addClass('button button-small button-color button-square-left btn btn-default');
        this.btn2.click(function (event) {
           self.init(); 
        });
        this.btn3 = $('<button>&nbsp;Save&nbsp;</button>');
        this.btn3.addClass('button button-small button-color button-square-right btn btn-brown');
        this.btn3.click(function (event) {
           self.save(); 
        });
        this.btn4 = $('<button></button>');
        this.btn4.addClass('button button-small button-assertive button-color button-square btn btn-danger buttonDrawColor');
        this.btn4.click(function (event) {
           self.signaturePad.penColor = "Crimson";
        });
        this.btn5 = $('<button></button>');
        this.btn5.addClass('button button-small button-positive button-color button-square btn btn-info buttonDrawColor');
        this.btn5.click(function (event) {
           self.signaturePad.penColor = "LightSeaGreen";
        });
        this.btn6 = $('<button></button>');
        this.btn6.addClass('button button-small button-balanced button-color button-square btn btn-success buttonDrawColor');
        this.btn6.click(function (event) {
            self.signaturePad.penColor = "green";
        });
        this.btn7 = $('<button></button>');
        this.btn7.addClass('button button-small button-dark button-color button-square btn btn-dark buttonDrawColor');
        this.btn7.click(function (event) {
            self.signaturePad.penColor = "black";
        });

        this.btn1.after(btnGroup);
        btnGroup.append(this.btn2);
        btnGroup.append(this.btn4);
        btnGroup.append(this.btn5);
        btnGroup.append(this.btn6);
        btnGroup.append(this.btn7);
        btnGroup.append(this.btn3);
        this.btn1.hide();

        this.img.before('<canvas class="signature divcenter nodivcenter" id="signature'+this.idCont+'"'+
            ' width="'+this.widthImg+'px'+'"'+
            ' height="'+this.heightImg+'px'+'"> </canvas>');

        this.canvas = $("#signature"+this.idCont);
        this.signaturePad = new SignaturePad(this.canvas[0]);
        this.signaturePad.penColor = "Crimson";
        this.img.hide();
        this.signaturePad.fromDataURL(this.img.prop('src'));
    },
    save: function() {
        var self = this;
        var ctx = this.canvas[0].getContext("2d");
        this.urlImg = this.canvas[0].toDataURL();
        self.saveFunc(this.urlImg);
        this.init();
    },
    getImgSize: function() {
        var self = this;
        var newImg = new Image();
        newImg.onload = function() {
          self.heightImg = newImg.height;
          self.widthImg = newImg.width;
              
          //reajustamos
          var max = getMaxWidth();
          if(self.widthImg > max){
            self.heightImg = Math.round(self.heightImg*max/self.widthImg);
            self.widthImg = max;
          }
          console.debug(self.widthImg + ':' + self.heightImg);

          self.initContinue();
        }
        newImg.src = this.urlImg; // this must be done AFTER setting onload
    },
}
var getMaxWidth = function()
{
    var max = $(window).width()-34;
    if(max > 600) max = 600;
    return max;
}
var dashboardChart = function(data){
    var chart = {};
    chart.type = "ColumnChart";
    chart.data = data
    chart.options = {
        "title": "",
        "isStacked": true,
       // "legend": "none",
      
      "legend": {
                "textStyle":
                {
                  color: "#000000",
                  fontName: "Arial",
                  fontSize: "11",
                  bold: false,
                  italic: false
                }

       },
        "displayExactValues": true,
        "vAxis": {
            "gridlines": {"count": 3, "color": "white"},
            "textStyle":
                {
                  color: "#000000",
                  fontName: "Arial",
                  fontSize: "12",
                  bold: false,
                  italic: false
                }

         },
         "hAxis": {
            "textStyle":
                {
                  color: "#000000",
                  fontName: "Arial",
                  fontSize: "12",
                  bold: false,
                  italic: false
                }

         },

        "tooltip": {"isHtml": true },
        "colors": [
            '#d34439', 
            '#4bbddc',
            '#dddddd', 
        ]
    };
    
    return chart;
}

//MAQUETA//


function seconds2time (seconds) {

    var hours   = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var seconds = seconds - (hours * 3600) - (minutes * 60);
    var time = "";

    if (hours != 0) {
      time = hours+":";
    }
    if (minutes != 0 || time !== "") {
      minutes = (minutes < 10 && time !== "") ? "0"+minutes : String(minutes);
      time += minutes+":";
    }
    else{
      time += '0:';
    }
    if (time === "") {
      time = seconds+"";
    }
    else {
      time += (seconds < 10) ? "0"+seconds : String(seconds);
    }
    return time;
}

var openSelect = function(selector){
     var element = $(selector)[0], worked = false;
    if (document.createEvent) { // all browsers
        var e = document.createEvent("MouseEvents");
        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        worked = element.dispatchEvent(e);
    } else if (element.fireEvent) { // ie
        worked = element.fireEvent("onmousedown");
    }
    if (!worked) { // unknown browser / error
        alert("It didn't worked in your browser.");
    }   
}

function radiobutton()
{
    //console.debug($('input[type=radio]'));

    $('input[type=radio]').iCheck({
        //checkboxClass: 'icheckbox_square',
        radioClass: 'iradio_square-blue',
        //increaseArea: '20%' // optional
    });
}

var dateToSQL = function(orig_date){
  //console.log(orig_date);
  var date = new String(orig_date);
  var dateArr = date.split("/"); 
  //console.debug(dateArr);

  return dateArr[2]+"-"+dateArr[1]+"-"+dateArr[0];
}

var calendarString = function(cal){
  var r = '';
  if(cal.date_in)
    r+='Starting on '+cal.date_in+". ";
  if(cal.recurrence && cal.each > 0){
    r+="Repeat each ";
    if(cal.each > 1)
      r+= cal.each+" ";

    r+= cal.repeat+(cal.each > 1?"s":'')+" ";

    if(cal.repeat == "week"){
      if(cal.noDayArr || !cal.days_week){
        cal.days_week = [];

        var days = ['L','M','X','J','V','S','D'];
        for (var i = 0; i < days.length; i++) {

          if(cal[days[i]])
            cal.days_week.push(days[i]);
        };
      }
      else if(!angular.isArray(cal.days_week))
        cal.days_week = cal.days_week.split(",");

      if(cal.days_week.length==7 && cal.each==1){
        r = r.slice(0, -10);
        r += "everyday"
      }
      else if(cal.days_week.length > 0){
        r+= "on every ";
        for (var i = 0; i < cal.days_week.length; i++) {
          if(i>0 && i==cal.days_week.length-1){
            r = r.slice(0, -2);
            r += " and ";
          }
          switch(cal.days_week[i]){
            case 'L': r+= "moday, "; break;
            case 'M': r+= "tuesday, "; break;
            case 'X': r+= "wednesday, "; break;
            case 'J': r+= "thursday, "; break;
            case 'V': r+= "friday, "; break;
            case 'S': r+= "saturday, "; break;
            case 'D': r+= "sunday, "; break;
          }
        };
        r = r.slice(0, -2);
      }
      
    }
    if(cal.finish){
      if(cal.finish == '1' && cal.out_after_repetitions > 0){
        r+= " for "+cal.out_after_repetitions+" times."; 
        delete cal.date_out;
      }
      else if(cal.finish == '2' && cal.date_out != null){
        r+= " until "+cal.date_out+".";
        delete cal.out_after_repetitions;
      }
      else r+= ".";
    }
    else{
      if(cal.out_after_repetitions > 0)
        r+= cal.out_after_repetitions+" times.";
      else if(cal.date_out != null)
        r+= "until "+cal.date_out+"."; 
    }
  }
  return r;
}


var insertItemInArray = function (index, item, array) {
  array.splice(index, 0, item);
};



/*
function rollover()
{
    $("img.rollover").hover(function(){
        $(this).attr('src', $(this).attr('src').replace(/off_24x24\.png$/, 'on_24x24.png'));
    }, 
    function(){
        $(this).attr('src', $(this).attr('src').replace(/on_24x24\.png$/, 'off_24x24.png'));
    }
    );
}


function tags()
{
    $(function() {
        var availableTags = [
            "Bar",
            "Hotel",
            "Coffee",
            "Store",
            "Panaderia"
            ];
        $( "input#tags" ).autocomplete({
        source: availableTags
        });
    });
}

function spinner()
{

    // Bind normal buttons
    Ladda.bind( 'button.ladda-button', { timeout: 2000 } );

    // Bind progress buttons and simulate loading progress
    Ladda.bind( '.progress-demo button', {
        callback: function( instance ) {
            var progress = 0;
            var interval = setInterval( function() {
                progress = Math.min( progress + Math.random() * 0.1, 1 );
                instance.setProgress( progress );

                if( progress === 1 ) {
                    instance.stop();
                    clearInterval( interval );
                }
            }, 200 );
        }
    } );

    // You can control loading explicitly using the JavaScript API
    // as outlined below:

    // var l = Ladda.create( document.querySelector( 'button' ) );
    // l.start();
    // l.stop();
    // l.toggle();
    // l.isLoading();
    // l.setProgress( 0-1 );

}


function tooltips()
{
    $("[rel='tooltip']").tooltip();
}


function issues()
{
    $('#issues').change(function() {
        $('#issuesdiv').toggle();
    });
}


function filtro()
{
    $('.filterOptions li a').click(function () {
        var ourClass = $(this).attr('class');
        $('.filterOptions li').removeClass('active');
        $(this).parent().addClass('active');

        if (ourClass == 'all') {
            $('#ourHolder').children().show();
        } else {
            $('#ourHolder').children('div:not(.' + ourClass + ')').hide();
            $('#ourHolder').children('div.' + ourClass).show();
        }

        return false;
    });
}


function customselect()
{
    $('select.select').each(function(){

        $('.caret2').hide();

        var title = $(this).attr('title');
        if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
        $(this)
            .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
            .after('<span class="select">' + title + '</span>')
            .change(function(){
                var val = $('option:selected',this).text();
                //var caret = '<span class=\"caret\"></span>';
                $(this).next().html('order by ' + '<em>' + val + '</em>');
                $('.caret2').show();
            })
    });
}
*/
