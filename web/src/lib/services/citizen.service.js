ibhdFtry.CitizenService = function($rootScope, $q, ParseService){
	return {
		sample: [
			{
				name:"Alvaro Yuste Torregrosa",
				image:"https://media.licdn.com/mpr/mpr/shrink_200_200/p/1/005/02e/303/02e1b01.jpg",
				location:"Alicante",
				position:"Software Developer",
				valorations: 323,
				gender: 1,
				age: 34,
				points: 32
			},
			{
				name:"Pablo Pernías",
				image:"https://lh4.googleusercontent.com/-WiGp2wMvm4Q/AAAAAAAAAAI/AAAAAAAAOd8/XDYJTc2FUzo/photo.jpg",
				location:"San Vicente",
				position:"Software Developer",
				valorations: 123,
				gender: 1,
				age: 30,
				points: 67
			},
			{
				name:"Roxanne López van Doreen",
				image:"https://avatars2.githubusercontent.com/u/7715594?v=3&s=460",
				location:"Barcelona",
				position:"Software Developer",
				valorations: 423,
				gender: 0,
				age: 25,
				points: 89
			},
			{
				name:"Jorge Soler",
				image: "http://www.primeraedicionweb.com.ar/mundial/wp-content/uploads/2014/06/pepe_reina_1379045498.jpg",
				location:"Valencia",
				position:"Software Developer",
				valorations: 223,
				gender: 1,
				age: 40,
				points: 60
			},
			{
				name:"Pablo Niñoles",
				image:"http://prendedor.es/wp-content/uploads/2014/03/pablo-ni%C3%B1oles-aznar-pniaz-prendedor.es_-300x300.png",
				location:"Albacete",
				position:"Software Developer",
				valorations: 1233,
				gender: 1,
				age: 35,
				points: 93
			},
			{
				name:"Alvaro Yuste Torregrosa",
				image:"https://media.licdn.com/mpr/mpr/shrink_200_200/p/1/005/02e/303/02e1b01.jpg",
				location:"Alicante",
				position:"Software Developer",
				valorations: 233,
				gender: 1,
				age: 34,
				points: 32
			},
			{
				name:"Pablo Pernías",
				image:"https://lh4.googleusercontent.com/-WiGp2wMvm4Q/AAAAAAAAAAI/AAAAAAAAOd8/XDYJTc2FUzo/photo.jpg",
				location:"San Vicente",
				position:"Software Developer",
				valorations: 123,
				gender: 1,
				age: 30,
				points: 67
			},
			{
				name:"Roxanne López van Doreen",
				image:"https://avatars2.githubusercontent.com/u/7715594?v=3&s=460",
				location:"Barcelona",
				position:"Software Developer",
				valorations: 173,
				gender: 0,
				age: 25,
				points: 89
			},
			{
				name:"Jorge Soler",
				image: "http://www.primeraedicionweb.com.ar/mundial/wp-content/uploads/2014/06/pepe_reina_1379045498.jpg",
				location:"Valencia",
				position:"Software Developer",
				valorations: 163,
				gender: 1,
				age: 40,
				points: 60
			},
			{
				name:"Pablo Niñoles",
				image:"http://prendedor.es/wp-content/uploads/2014/03/pablo-ni%C3%B1oles-aznar-pniaz-prendedor.es_-300x300.png",
				location:"Albacete",
				position:"Software Developer",
				valorations: 375,
				gender: 1,
				age: 35,
				points: 93
			},
			{
				name:"Alvaro Yuste Torregrosa",
				image:"https://media.licdn.com/mpr/mpr/shrink_200_200/p/1/005/02e/303/02e1b01.jpg",
				location:"Alicante",
				position:"Software Developer",
				valorations: 221,
				gender: 1,
				age: 34,
				points: 32
			},
			{
				name:"Pablo Pernías",
				image:"https://lh4.googleusercontent.com/-WiGp2wMvm4Q/AAAAAAAAAAI/AAAAAAAAOd8/XDYJTc2FUzo/photo.jpg",
				location:"San Vicente",
				position:"Software Developer",
				valorations: 1314,
				gender: 1,
				age: 30,
				points: 67
			},
			{
				name:"Roxanne López van Doreen",
				image:"https://avatars2.githubusercontent.com/u/7715594?v=3&s=460",
				location:"Barcelona",
				position:"Software Developer",
				valorations: 1432,
				gender: 0,
				age: 25,
				points: 89
			},
			{
				name:"Jorge Soler",
				image: "http://www.primeraedicionweb.com.ar/mundial/wp-content/uploads/2014/06/pepe_reina_1379045498.jpg",
				location:"Valencia",
				position:"Software Developer",
				valorations: 145,
				gender: 1,
				age: 40,
				points: 60
			},
			{
				name:"Pablo Niñoles",
				image:"http://prendedor.es/wp-content/uploads/2014/03/pablo-ni%C3%B1oles-aznar-pniaz-prendedor.es_-300x300.png",
				location:"Albacete",
				position:"Software Developer",
				valorations: 1234,
				gender: 1,
				age: 35,
				points: 93
			},
			{
				name:"Alvaro Yuste Torregrosa",
				image:"https://media.licdn.com/mpr/mpr/shrink_200_200/p/1/005/02e/303/02e1b01.jpg",
				location:"Alicante",
				position:"Software Developer",
				valorations: 56,
				gender: 1,
				age: 34,
				points: 32
			},
			{
				name:"Pablo Pernías",
				image:"https://lh4.googleusercontent.com/-WiGp2wMvm4Q/AAAAAAAAAAI/AAAAAAAAOd8/XDYJTc2FUzo/photo.jpg",
				location:"San Vicente",
				position:"Software Developer",
				valorations: 523,
				gender: 1,
				age: 30,
				points: 67
			},
			{
				name:"Roxanne López van Doreen",
				image:"https://avatars2.githubusercontent.com/u/7715594?v=3&s=460",
				location:"Barcelona",
				position:"Software Developer",
				valorations: 12331,
				gender: 0,
				age: 25,
				points: 89
			},
			{
				name:"Jorge Soler",
				image: "http://www.primeraedicionweb.com.ar/mundial/wp-content/uploads/2014/06/pepe_reina_1379045498.jpg",
				location:"Valencia",
				position:"Software Developer",
				valorations: 123,
				gender: 1,
				age: 40,
				points: 60
			},
			{
				name:"Pablo Niñoles",
				image:"http://prendedor.es/wp-content/uploads/2014/03/pablo-ni%C3%B1oles-aznar-pniaz-prendedor.es_-300x300.png",
				location:"Albacete",
				position:"Software Developer",
				valorations: 23,
				gender: 1,
				age: 35,
				points: 93
			}
		],

		list: function(){
			return this.sample;
		},
	}
}