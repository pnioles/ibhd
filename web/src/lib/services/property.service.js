ibhdFtry.PropertyService = function($rootScope, $q, ParseService){
	return {
		detail: function(code){
			var q = $q.defer();
			var query = new Parse.Query(ParseService.Property);
			query.equalTo("code", code);
			//console.log("code", code);
			query.find({
			  success: function(result) {/*console.log(result);*/ q.resolve(result[0]); },
			  error: function(error) {console.log(error); q.reject(error); }
			});
			return q.promise;

		},
		list: function(params){
			var q = $q.defer();
			var query = new Parse.Query(ParseService.Property);
			query.limit(20);
			query.find({
			  success: function(result) {
			  	for(var i in result){
			  		if(!result[i].get("image"))
			  			result[i].set("image", $rootScope.getImageRandom());

			  		if(!result[i].get("valorations"))
						result[i].set("valorations", $rootScope.getRandom(3000));
			  		
			  		result[i].save();
			  		result[i] = angular.copy(result[i].attributes);
			  	}
			  	//console.log(result); 
			  	q.resolve(result);
			  },
			  error: function(error) {console.log(error); q.reject(error); }
			});
			return q.promise;
		},
	}
}