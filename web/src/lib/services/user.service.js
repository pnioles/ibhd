ibhdFtry.UserService = function($rootScope, $q, ParseService){
	return {
		getCurrent: function(){
			return Parse.User.current();
		},
		signup: function(objVars){
			var q = $q.defer();

			if(!objVars['email'] || objVars['email'] == '' || !validEmail(objVars['email']))
				q.reject({message: 'Please enter a valid email'});

			var user = new ParseService.User();

			user.set("username", objVars['email']);
			user.set("name", objVars['name']);
			user.set("email", objVars['email']);
			user.set("password", objVars['pass']);

			user.signUp(null, {
				success: function(user) {q.resolve(user); },
				error: function(user, error) {q.reject(error); }
			});
			return q.promise;
		},
		login: function(name, pass){
			var q = $q.defer();
			ParseService.User.logIn(name, pass, {
				success: function(user) {q.resolve(user); },
				error: function(user, error) {q.reject(error); }
			});
			return q.promise;
		},
		logout: function(){
			ParseService.User.logOut();
		},
		resetPass: function(email){
			var q = $q.defer();
			if(!email || email == '' || !validEmail(email))
				q.reject({message: 'Please enter a valid email'});

			Parse.User.requestPasswordReset(email , {
				success: function(user) {q.resolve(user); },
				error: function(error) {q.reject(error); }
			});
			return q.promise;
		},
		update: function(objVars){
			var q = $q.defer();
			var user = this.getCurrent();

			if(user.get("name")!=objVars['name'])
				user.set("name", objVars['name']);

			if(user.get("email")!=objVars['email'])
				user.set("email", objVars['email']);

			if(user.get("username")!=objVars['email'])
				user.set("username", objVars['email']);

			console.log(user.get('type_service'));

			user.save(null, {
				success: function(user) {q.resolve(user); },
				error: function(user, error) {q.reject(error);}
			})
			return q.promise;
		},
		detail: function(id){
			var q = $q.defer();
			var query = new Parse.Query(ParseService.User);
			query.equalTo("objectId", id);
			query.find({
			  success: function(result) {q.resolve(result[0]); },
			  error: function(error) {q.reject(error); }
			});
			return q.promise;
		}
	}
}