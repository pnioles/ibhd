
ibhdFiltr.capitalize = function() {
    return function(input, scope) {
        if (input) return input.substring(0,1).toUpperCase()+input.substring(1);
    }
};

ibhdFiltr.formatDate = function($rootScope) {
    return function(input, scope) {
    	//console.debug($rootScope);
        //return $rootScope.user.culture.iso+": "+input;
        if(input)
        	return input.substring(input.length-3,0);
       	else return null;
    }
};

ibhdFiltr.formatTime = function($rootScope) {
    return function(input, scope) {
    	//console.debug($rootScope);
        //return $rootScope.user.culture.iso+": "+input;
		if(input)
        	return input.substring(input.length-3,0);
       	else return null;
    }
};