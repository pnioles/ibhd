ibhdCtrl.HouseDetailController = function($scope, PropertyService, $rootScope){
	$scope.house = {};

	$scope.selectedIndex = 0;
    $scope.next = function() {$scope.selectedIndex = Math.min($scope.selectedIndex + 1, 2) ; };
    $scope.previous = function() {$scope.selectedIndex = Math.max($scope.selectedIndex - 1, 0); };

    var oldValue;

	PropertyService.detail(135178).then(
		function(property){ $scope.house = property},
		function(error){ console.log("ERROR: ", error)}
	);

	var ranges = [
            [1246406400000, 154300, 167700],
            [1246492800000, 154500, 167800],
            [1246579200000, 155500, 169600],
            [1246665600000, 156700, 170700],
            [1246752000000, 156500, 165000],
            [1246838400000, 157800, 165700],
            [1246924800000, 153500, 164800],
            [1247011200000, 150500, 161400],
            [1247097600000, 144200, 163800],
        ],
        averages = [
            [1246406400000, 164500],
            [1246492800000, 162100],
            [1246579200000, 163000],
            [1246665600000, 163800],
            [1246752000000, 161400],
            [1246838400000, 161300],
            [1246924800000, 158300],
            [1247011200000, 155400],
            [1247097600000, 156400]
        ];

	$scope.saved = false;
	$scope.setSaved = function(value){
		$scope.saved = value;
	}

	$scope.citizens = [
		{
			name:"Alvaro Yuste Torregrosa",
			image:"https://media.licdn.com/mpr/mpr/shrink_200_200/p/1/005/02e/303/02e1b01.jpg",
			location:"Alicante",
			position:"Software Developer",
			valorations: 323,
			gender: 1,
			age: 34,
			points: 32
		},
		{
			name:"Pablo Pernías",
			image:"https://lh4.googleusercontent.com/-WiGp2wMvm4Q/AAAAAAAAAAI/AAAAAAAAOd8/XDYJTc2FUzo/photo.jpg",
			location:"San Vicente",
			position:"Software Developer",
			valorations: 123,
			gender: 1,
			age: 30,
			points: 67
		},
		{
			name:"Roxanne López van Doreen",
			image:"https://avatars2.githubusercontent.com/u/7715594?v=3&s=460",
			location:"Barcelona",
			position:"Software Developer",
			valorations: 423,
			gender: 0,
			age: 25,
			points: 89
		},
		{
			name:"Jorge Soler",
			image: "http://www.primeraedicionweb.com.ar/mundial/wp-content/uploads/2014/06/pepe_reina_1379045498.jpg",
			location:"Valencia",
			position:"Software Developer",
			valorations: 223,
			gender: 1,
			age: 40,
			points: 60
		},
		{
			name:"Pablo Niñoles",
			image:"http://prendedor.es/wp-content/uploads/2014/03/pablo-ni%C3%B1oles-aznar-pniaz-prendedor.es_-300x300.png",
			location:"Albacete",
			position:"Software Developer",
			valorations: 1233,
			gender: 1,
			age: 35,
			points: 93
		}
	];

	$scope.valueChartConfig = {
		options: {
	      chart: {
	          type: 'column'
	      },
	      tooltip: {
	          style: {
	              padding: 10,
	              fontWeight: 'bold'
	          }
	      }
	  	},
	  	title: {
	     text: 'Valoraciones en función del precio final'
	  	},
	  	loading: false,
		xAxis: {
			categories: ['135K-140K', '140K-145K', '145K-150K', '150K-155K', 
			'155K-160K', '160K-165K', '165K-170K'],
			title: {text: 'Precio (€)'}
		},
		yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Puntuacion (%)'
            }
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        series: [{
        	name: 'Intervalos de Precio',
            data: [14, 34, 50, 55, 72, 99, 56]

        }]
	};

	$scope.timeChartConfig = {
		options: {
	      tooltip: {
	         	crosshairs: true,
            	shared: true,
            	valueSuffix: '€'
	      }
	  	},
	  	title: {
	     text: 'Precio idóneo en función del tiempo'
	  	},
	  	xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        series: [{
            name: 'Precio',
            data: averages,
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        }, {
            name: 'Intervalos',
            data: ranges,
            type: 'arearange',
            lineWidth: 0,
            linkedTo: ':previous',
            color: Highcharts.getOptions().colors[0],
            fillOpacity: 0.3,
            zIndex: 0
        }]

	};
	$scope.neighborhoods = ["Alicante","San Juan","Elche","Benidorm","Murcia","Villena"];
	$scope.neighborhood = null;
	$scope.mediaM = 154.43;
	$scope.moda = 160.77;
	$scope.mediana = 158.55;


	$scope.change = function(nb){
		if(nb == "Alicante"){
			$scope.data1.series[0].data = [105, 100, 140, 168, 150, 115];
			$scope.mediaM = 128.21;
			$scope.moda = 130.98;
			$scope.mediana = 132.25;
		}
		else if(nb == "Villena"){
			$scope.data1.series[0].data = [110.2, 112.0, 139.0, 178.4, 175.1, 148.6, 120.4];
			$scope.mediaM = 138.29;
			$scope.moda = 142.40;
			$scope.mediana = 148.65;
		}
		else if(nb != null){
			$scope.data1.series[0].data = [80, 95, 124, 155, 138, 100];
			$scope.mediaM = 98.88;
			$scope.moda = 101.23;
			$scope.mediana = 105.35;
		}	
		

		console.log(nb);
		

	};

	$scope.data1 = {
		series: [{
			name: 'Intervalos de Precio',
            data: [120.2, 125.0, 156.0, 186.4, 184.1, 155.6, 124.4]
        }],
        categories: {categories:['135k-140k', '140k-145k', '145k-150k', '150k-155k', '155k-160k', '160k-165k', '165k-170k','170k-175k','175k-180k']}
	}

	$scope.chartConfig = {
		yAxis: {
            title: {
                text: 'Personas (en miles)'
            }
        },
		title: {
            text: 'Número de valoraciones por precio'
        },
        xAxis: $scope.data1.categories,
        series: $scope.data1.series
	}

	$scope.initMap = function(){
		var myLatlng = new google.maps.LatLng(38.742268,-0.009516);
		var mapOptions = {
		  zoom: 18,
		  center: myLatlng
		}
		var map = new google.maps.Map(document.getElementById('map'), mapOptions);

		var marker = new google.maps.Marker({
		    position: myLatlng,
		    map: map,
		    title: 'Bungalows del Pinar'
		});
	}
}