ibhdCtrl.HouseListController = function($scope, $rootScope, PropertyService){
	$scope.houses = [];
	PropertyService.list().then(
		function(properties){$scope.houses = properties},
		function(error){console.log("ERROR: ", error)}
	);
}