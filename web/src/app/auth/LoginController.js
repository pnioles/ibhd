ibhdCtrl.LoginController = function($rootScope, $scope, $state, $location, $timeout, UserService){
  $scope.user = {};
  $scope.showPass = false;

  $scope.login = function(){
  	$scope.loadingLogin = true;
  	UserService.login($scope.user.email, $scope.user.pass).then(
  		function(restaurant){
  			$state.go('app.dashboard');
  		},
  		function(error){
  			$scope.loadingLogin = false;
  			$scope.loadingSignup = false;
  			$scope.alert = error.message;
        errorToast('Error en el login');
  		} 
  	);
  }

  $scope.signup = function(){
  	$scope.loadingSignup = true;

    UserService.signup({
      name: $scope.user.name,
      email: $scope.user.email,
      pass: $scope.user.pass
    }).then(
      function(success){$scope.login();},
      function(error){$scope.loadingSignup = false; $scope.alert = error.message; errorToast('Error en el registro'); }
    );
  }

  $scope.resetpass = function(){
  	$scope.loadingReset = true;

  	UserService.resetPass($scope.user.email).then(
  		function(success){
  			$state.go('login');
  		},
  		function(error){
  			$scope.loadingReset = false;
  			$scope.alert = error.message;
        errorToast('Error reseteando contraseña');
  		} 
  	);
  }
}