ibhdCtrl.AppController = function($scope,$rootScope, $state, UserService){
	$rootScope.menuItemSelected = '';
	$scope.logout = function(){
	    UserService.logout();
	    $state.go('login');
	}
}