ibhdCtrl.UserController = function($scope,$rootScope, $location, CitizenService){
	$rootScope.menuItemSelected = 'Comunidad';

	$scope.citizens = CitizenService.list();

	$scope.greaterThan = function(prop, val){
		return function(item){
			if (!val || item[prop] > val) return true;
		}
	}
}
