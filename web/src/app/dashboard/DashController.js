ibhdCtrl.DashController = function($scope, $location, $rootScope, $mdDialog, $mdToast){
  $rootScope.menuItemSelected = 'Inicio';

  $scope.alert = '';
  $scope.tags = [
    {titulo: "Alicante",
    color: "#E2A687"
    },
    {titulo: "Costa",
    color: "#91E28F"
    },
    {titulo: ">20.000€",
    color: "#B690E2"
    }

  ];


	$scope.viviendas = [ //Redefinir con parametros...
      {
        img : 'assets/img/vivienda_1.jpg',
        title: 'Alicante San Juan 23.000€'
      },
      {
        img : 'assets/img/vivienda_2.jpg',
        title: 'Alicante Muchamiel 100.000€'
      },
      {
        img : 'assets/img/vivienda_3.jpg',
        title: 'Alicante Benidorm 250.000€'
      },
      {
        img : 'assets/img/vivienda_4.jpg',
        title: 'Murcia Caravaca 100.000€'
      },
      {
        img : 'assets/img/vivienda_2.jpg',
        title: 'Alicante Coveta 210.000€'
      }
  ];
  $scope.editData = function(ev) {
    $mdDialog.show({
      controller: DialogController,
      templateUrl: 'src/app/dashboard/dialog_modify.html',
      targetEvent: ev,
      locals: { scope: $scope },
    })
    .then(function(answer) {
      if(answer == 'confirmar'){
        $mdToast.show(
          $mdToast.simple()
            .content('Has modificado tu perfil con éxito.')
            .position("bottom right left")            
            .hideDelay(2000)
        );
      }
      else{
        /*$mdToast.show(
          $mdToast.simple()
            .content('Has cancelado la operación.')
            .position("bottom right left")            
            .hideDelay(3000)
        );*/

      }
      
    }, function() {
      
    });
  };

  function DialogController($scope, $mdDialog, $rootScope) {

    $scope.userToEdit = angular.copy($rootScope.mainUser);
    $scope.hide = function() {
      $mdDialog.hide();
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };
  }

}